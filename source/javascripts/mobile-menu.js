(function() {
  let mobileNavIsActive = false;
  const ul = document.getElementsByClassName("navigation-mobile__dropdown")[0]
  const div = document.getElementsByClassName("navigation-mobile")[0]
  
  //Event handler for clicking hamburger menu
  document.getElementById('slpMobileNavActive').onclick = () => {
    if(!mobileNavIsActive){
      ul.classList.remove("navigation-mobile__dropdown")
      ul.classList.add("navigation-mobile__dropdown-active")
      div.classList.remove("navigation-mobile")
      div.classList.add("navigation-mobile-active")
      
    }
    else {
      ul.classList.add("navigation-mobile__dropdown")
      ul.classList.remove("navigation-mobile__dropdown-active")
      div.classList.add("navigation-mobile")
      div.classList.remove("navigation-mobile-active")
    }
    mobileNavIsActive = !mobileNavIsActive
  }

  //Event Handler for Mobile Drop Downs
  const buttons = Array.from(document.querySelectorAll(".navigation-mobile__button"))
  const uls = buttons.map(element => element.nextElementSibling)

  let dropdownThatAreOpen = () => uls.filter(element => element.classList.contains('navigation-mobile__item-active'))

  buttons.forEach(element => element.onclick = () => {
    let current;
    if((current = dropdownThatAreOpen()).length === 1 && element.nextElementSibling !== current[0]){
      current[0].classList.add('navigation-mobile__item');
      current[0].classList.remove("navigation-mobile__item-active")
      current[0].previousElementSibling.lastElementChild.lastElementChild.style.transform = 'rotate(0deg)'
    }
    
    if(element.nextElementSibling.classList.contains('navigation-mobile__item')){
      element.nextElementSibling.classList.remove('navigation-mobile__item')
      element.nextElementSibling.classList.add("navigation-mobile__item-active")
      element.lastElementChild.lastElementChild.style.transform = "rotate(180deg)"
    }
    else{
      element.nextElementSibling.classList.add('navigation-mobile__item')
      element.nextElementSibling.classList.remove("navigation-mobile__item-active")
      element.lastElementChild.lastElementChild.style.transform = "rotate(0deg)"
    }

  });

})();
